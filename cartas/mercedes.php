<p>
	Este año voy tarde como acostumbro desde hace un tiempo…
	<br/><br/>Al igual que otros años, si tengo que hacer una valoración de este 2021, debo deciros que no me ha gustado nada de nada. Prefiero que se vaya, que se acabe ya….y que nos vengan cosas un poco mejores. Tampoco pido mucho, pero que sea un poco mejor. Eso dependerá en gran medida de cada uno de nosotros así que al menos, intentaré ponerle ganas.
	<br/><br/>La verdad que me cuesta ponerme a pensar qué cosas necesito ya que considero que tengo de todo, así que lo que os propongo es puro capricho.
	<br/><br/>Ideas:
</p>
    <ul>
		<?php
		//Esto es de otro año, pero para no perderlo...
		//<li>Alg&uacute;n perfume: os dejo unos cuantos de los que me gustan para pon&eacute;roslo m&aacute;s f&aacute;cil: Kingdom de Alexander Mcqueen, Zen de Shiseido o Narciso Rodriguez el del frasco rosa, o Euphoria de CK.</li>
		?>

		<li>Algo solidario, si puede ser algo para los damnificados de La Palma </li>
		<li>Auriculares tipo C, que los que tengo para el teléfono se me han estropeado.</li>
		<li>Un disco duro externo, que sea ligero y con capacidad. Que el que tengo pesa mucho y creo que en breve llegará a sus últimos días.</li>
		<li>Estas zapatillas  en talla 37: <a href="https://www.vans.es/shop/es/vans-es/zapatillas-safari-multi-old-skool-vn0a3wkt9xb#hero=0" target="_blank">https://www.vans.es/shop/es/vans-es/zapatillas-safari-multi-old-skool-vn0a3wkt9xb#hero=0</a></li>

    </ul>
<p>Con todas estas sugerencias espero poder haberos ayudado. Aunque de verdad, que no necesito nada de nada.  Espero que vuestra tarea sea llevadera y os deseo lo mejor para el 2022.</p>
<p>Mercedes</p>
