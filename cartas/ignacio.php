<p>&nbsp;&nbsp;&nbsp;Otro año más de comportamiento casi impecable para llegar a estas fecha con el marcador al máximo, pero por suerte tengo cubiertas todas mis necesidades, y además la mayoría de mis caprichos. No puedo quejarme de cómo van las cosas alrededor, somos afortunados y tenemos que agradecer todo lo bueno que nos pasa y la gente que tenemos para compartirlo.</p>
<p>&nbsp;&nbsp;&nbsp;Lo cierto es que tengo pocas ideas que dar, y parte de lo que voy a pedir es inmaterial, porque de algún modo hay que ayudar a que sus majestades a afrontar estos días. Por ello, estas son mi ideas para este año:</p>
<ul>
    <li>
        Quiero una especie de "legados de información" de algunos objetos, guardados en voz en la gabadora del móvil. Éstos son los elementos solicitados de momento:
        <ul>
            <li>Historia completa alrededor del abecedario de Pita hecho en punto de cruz.</li>
            <li>Historia completa alrededor de la cama del ¿S.XIX?.</li>
            <li>Una grabación de lo que sería una llamada de buenas noches de un día cualquiera.</li>
        </ul>
    </li>
    <li>Un plato de ducha en Cristóbal Bordiú. No quiero seguir dando más vueltas al tema y tener que vivir con una preocupación que no es necesaria.</li>
    <li>Un plato de LPs que permitar reproducir y convertir vinilos. Algo no muy complejo, del estilo de lo siguiente: <a href="https://www.amazon.es/DIGITNOW-Tocadiscos-Bluetooth-Seleccionables-Codificación/dp/B01LN4Z6F0/ref=sr_1_5_mod_primary_new?crid=3UUAS58K3SVPS&dib=eyJ2IjoiMSJ9.fnSeVT9xX8LQtXdCI70dHUS4AXm5ohb7mhSOA7jIQ1_Hk3axrQJw1sVki9JoEdoB-TTV_gg8eah4vpZaS1xq2FPlBwtLBaHKKT12YFIvMAyq0StSQmcl8vakSlZsoWWyBJ1mDRnQeCgCofuu1sikp8pSL6FHDuOrXAL_N390Bye5ktDV5TOXR8guXSmrBF3Ids538zgf9oXCA3-ztMCaWlEOdnF_6VClFWE_Ql3OQFd96LwMcPUbK5fY4bh-VXBs2Xd5xnLnaft2bLDJ6z0eHLVmyXEOkbeirCs_s3onAt4.8dDNsO0gnRX5kfNM5iop0QFJuzIiC5n8Zkm24k5NHTg&dib_tag=se&keywords=reproductor+vinilo+audio+technica+lp+60&nsdOptOutParam=true&qid=1734951440&sbo=RZvfv%2F%2FHxDF%2BO5021pAnSA%3D%3D&sprefix=reproductor+lp%2Caps%2C117&sr=8-5" target="_blank">Reproductor LP en Amazon</a></li>
    <li>Un cordón (bueno, dos) para la pala de pádel. El que viene no me gusta nada y no me resulta cómodo. Quiero cambiarlo por alguno de lo que tiene Babolat o Gladiator Padel en Amazon.</li>
    <li>Me encantaría ir a una final de padel de esas que casi siempre juegan Coello-Tapia vs Galán-Chingoto y verlos en directo. No sé cuando será la siguiente, que cada vez juegan más fuera de España.</li>
    <li>Me gustaría mucho encontrar una afición que no robe demasiado tiempo de estudio y que permita compartir tiempo de forma regular con Mercedes.</li>
    <li>Un casco mejorado para llevar a Alicante y que se me vea mejor, que el de allí esta bastante maltrecho. <a href="https://www.amazon.es/dp/B0DF7HBFYY/?coliid=I12UTB116NMKFO&colid=2AP6IBMA9FSYK&ref_=lv_ov_lig_dp_it&th=1&psc=1" target="_blank">Caso cómodo.</a></li>
</ul>
<p>&nbsp;&nbsp;&nbsp;Como siempre, me gustan las sorpresas, y los regalos que son resultado de la magia. Este año no necesito prácticamente nada.</p>
<p>Gracias y feliz año<br>Ignacio</p>